/*
 * xilinx_spi.c
 *
 * Xilinx SPI controller driver (master mode only)
 *
 * Author: MontaVista Software, Inc.
 *	source@mvista.com
 *
 * 2002-2007 (c) MontaVista Software, Inc.  This file is licensed under the
 * terms of the GNU General Public License version 2.  This program is licensed
 * "as is" without any warranty of any kind, whether express or implied.
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/interrupt.h>

#include <linux/spi/spi.h>
#include <linux/spi/spi_bitbang.h>
#include <linux/io.h>

#include "xilinx_spi.h"

struct xilinx_spi {
	/* bitbang has to be first */
	struct spi_bitbang bitbang;
	struct completion done;
	struct resource mem; /* phys mem */
	void __iomem	*regs;	/* virt. address of the control registers */
	u32 irq;
	u8 *rx_ptr;		/* pointer in the Tx buffer */
	const u8 *tx_ptr;	/* pointer in the Rx buffer */
	int remaining_bytes;	/* the number of bytes left to transfer */
	/* offset to the XSPI regs, these might vary... */
	u8 bits_per_word;
	bool big_endian;	/* The device could be accessed big or little
				 * endian
				 */
};


/* Register definitions as per "OPB Serial Peripheral Interface (SPI) (v1.00e)
 * Product Specification", DS464
 */
#define XSPI_CR_OFFSET		0x60	/* Control Register */

#define XSPI_CR_ENABLE		0x02
#define XSPI_CR_MASTER_MODE	0x04
#define XSPI_CR_CPOL		0x08
#define XSPI_CR_CPHA		0x10
#define XSPI_CR_MODE_MASK	(XSPI_CR_CPHA | XSPI_CR_CPOL)
#define XSPI_CR_TXFIFO_RESET	0x20
#define XSPI_CR_RXFIFO_RESET	0x40
#define XSPI_CR_MANUAL_SSELECT	0x80
#define XSPI_CR_TRANS_INHIBIT	0x100
#define XSPI_CR_LSB_FIRST	0x200

#define XSPI_SR_OFFSET		0x64	/* Status Register */

#define XSPI_SR_RX_EMPTY_MASK	0x01	/* Receive FIFO is empty */
#define XSPI_SR_RX_FULL_MASK	0x02	/* Receive FIFO is full */
#define XSPI_SR_TX_EMPTY_MASK	0x04	/* Transmit FIFO is empty */
#define XSPI_SR_TX_FULL_MASK	0x08	/* Transmit FIFO is full */
#define XSPI_SR_MODE_FAULT_MASK	0x10	/* Mode fault error */

#define XSPI_TXD_OFFSET		0x68	/* Data Transmit Register */
#define XSPI_RXD_OFFSET		0x6C	/* Data Receive Register */

#define XSPI_SSR_OFFSET		0x70	/* 32-bit Slave Select Register */

/* Register definitions as per "OPB IPIF (v3.01c) Product Specification", DS414
 * IPIF registers are 32 bit
 */
#define XIPIF_V123B_DGIER_OFFSET	0x1c	/* IPIF global int enable reg */
#define XIPIF_V123B_GINTR_ENABLE	0x80000000

#define XIPIF_V123B_IISR_OFFSET		0x20	/* IPIF interrupt status reg */
#define XIPIF_V123B_IIER_OFFSET		0x28	/* IPIF interrupt enable reg */

#define XSPI_INTR_MODE_FAULT		0x01	/* Mode fault error */
#define XSPI_INTR_SLAVE_MODE_FAULT	0x02	/* Selected as slave while
						 * disabled */
#define XSPI_INTR_TX_EMPTY		0x04	/* TxFIFO is empty */
#define XSPI_INTR_TX_UNDERRUN		0x08	/* TxFIFO was underrun */
#define XSPI_INTR_RX_FULL		0x10	/* RxFIFO is full */
#define XSPI_INTR_RX_OVERRUN		0x20	/* RxFIFO was overrun */
#define XSPI_INTR_TX_HALF_EMPTY		0x40	/* TxFIFO is half empty */

#define XIPIF_V123B_RESETR_OFFSET	0x40	/* IPIF reset register */
#define XIPIF_V123B_RESET_MASK		0x0a	/* the value to write */

/* to follow are some functions that does little of big endian read and
 * write depending on the config of the device.
 */
static inline void xspi_write8(struct xilinx_spi *xspi, u32 offs, u8 val)
{
	iowrite8(val, xspi->regs + offs + ((xspi->big_endian) ? 3 : 0));
}

static inline void xspi_write16(struct xilinx_spi *xspi, u32 offs, u16 val)
{
	if (xspi->big_endian)
		iowrite16be(val, xspi->regs + offs + 2);
	else
		iowrite16(val, xspi->regs + offs);
}

static inline void xspi_write32(struct xilinx_spi *xspi, u32 offs, u32 val)
{
	if (xspi->big_endian)
		iowrite32be(val, xspi->regs + offs);
	else
		iowrite32(val, xspi->regs + offs);
}

static inline u8 xspi_read8(struct xilinx_spi *xspi, u32 offs)
{
	return ioread8(xspi->regs + offs + ((xspi->big_endian) ? 3 : 0));
}

static inline u16 xspi_read16(struct xilinx_spi *xspi, u32 offs)
{
	if (xspi->big_endian)
		return ioread16be(xspi->regs + offs + 2);
	else
		return ioread16(xspi->regs + offs);
}

static inline u32 xspi_read32(struct xilinx_spi *xspi, u32 offs)
{
	if (xspi->big_endian)
		return ioread32be(xspi->regs + offs);
	else
		return ioread32(xspi->regs + offs);
}

static void xspi_init_hw(struct xilinx_spi *xspi)
{
	/* Reset the SPI device */
	xspi_write32(xspi, XIPIF_V123B_RESETR_OFFSET, XIPIF_V123B_RESET_MASK);
	/* Disable all the interrupts just in case */
	xspi_write32(xspi, XIPIF_V123B_IIER_OFFSET, 0);
	/* Enable the global IPIF interrupt */
	xspi_write32(xspi, XIPIF_V123B_DGIER_OFFSET, XIPIF_V123B_GINTR_ENABLE);
	/* Deselect the slave on the SPI bus */
	xspi_write32(xspi, XSPI_SSR_OFFSET, 0xffff);
	/* Disable the transmitter, enable Manual Slave Select Assertion,
	 * put SPI controller into master mode, and enable it */
	xspi_write16(xspi, XSPI_CR_OFFSET,
		 XSPI_CR_TRANS_INHIBIT | XSPI_CR_MANUAL_SSELECT |
		 XSPI_CR_MASTER_MODE | XSPI_CR_ENABLE | XSPI_CR_TXFIFO_RESET |
		 XSPI_CR_RXFIFO_RESET);
}

static void xilinx_spi_chipselect(struct spi_device *spi, int is_on)
{
	struct xilinx_spi *xspi = spi_master_get_devdata(spi->master);

	if (is_on == BITBANG_CS_INACTIVE) {
		/* Deselect the slave on the SPI bus */
		xspi_write32(xspi, XSPI_SSR_OFFSET, 0xffff);
	} else if (is_on == BITBANG_CS_ACTIVE) {
		/* Set the SPI clock phase and polarity */
		u16 cr = xspi_read16(xspi, XSPI_CR_OFFSET)
			 & ~XSPI_CR_MODE_MASK;
		if (spi->mode & SPI_CPHA)
			cr |= XSPI_CR_CPHA;
		if (spi->mode & SPI_CPOL)
			cr |= XSPI_CR_CPOL;
		xspi_write16(xspi, XSPI_CR_OFFSET, cr);

		/* We do not check spi->max_speed_hz here as the SPI clock
		 * frequency is not software programmable (the IP block design
		 * parameter)
		 */

		/* Activate the chip select */
		xspi_write32(xspi, XSPI_SSR_OFFSET,
			 ~(0x0001 << spi->chip_select));
	}
}

/* spi_bitbang requires custom setup_transfer() to be defined if there is a
 * custom txrx_bufs(). We have nothing to setup here as the SPI IP block
 * supports 8 or 16 bits per word, which can not be changed in software.
 * SPI clock can't be changed in software.
 * Check for correct bits per word. Chip select delay calculations could be
 * added here as soon as bitbang_work() can be made aware of the delay value.
 */
static int xilinx_spi_setup_transfer(struct spi_device *spi,
	struct spi_transfer *t)
{
	u8 bits_per_word;
	struct xilinx_spi *xspi = spi_master_get_devdata(spi->master);

	bits_per_word = (t->bits_per_word) ? t->bits_per_word :
		spi->bits_per_word;
	if (bits_per_word != xspi->bits_per_word) {
		dev_err(&spi->dev, "%s, unsupported bits_per_word=%d\n",
			__func__, bits_per_word);
		return -EINVAL;
	}

	return 0;
}

static int xilinx_spi_setup(struct spi_device *spi)
{
	/* always return 0, we can not check the number of bits.
	 * There are cases when SPI setup is called before any driver is
	 * there, in that case the SPI core defaults to 8 bits, which we
	 * do not support in some cases. But if we return an error, the
	 * SPI device would not be registered and no driver can get hold of it
	 * When the driver is there, it will call SPI setup again with the
	 * correct number of bits per transfer.
	 * If a driver setups with the wrong bit number, it will fail when
	 * it tries to do a transfer
	 */
	return 0;
}

static void xilinx_spi_fill_tx_fifo(struct xilinx_spi *xspi)
{
	u8 sr;
	u8 wsize;
	if (xspi->bits_per_word == 8)
		wsize = 1;
	else if (xspi->bits_per_word == 16)
		wsize = 2;
	else
		wsize = 4;

	/* Fill the Tx FIFO with as many bytes as possible */
	sr = xspi_read8(xspi, XSPI_SR_OFFSET);
	while ((sr & XSPI_SR_TX_FULL_MASK) == 0 &&
		xspi->remaining_bytes > 0) {
		if (xspi->tx_ptr) {
			if (wsize == 1)
				xspi_write8(xspi, XSPI_TXD_OFFSET,
					*xspi->tx_ptr);
			else if (wsize == 2)
				xspi_write16(xspi, XSPI_TXD_OFFSET,
					*(u16 *)(xspi->tx_ptr));
			else if (wsize == 4)
				xspi_write32(xspi, XSPI_TXD_OFFSET,
					*(u32 *)(xspi->tx_ptr));

			xspi->tx_ptr += wsize;
		} else
			xspi_write8(xspi, XSPI_TXD_OFFSET, 0);
		xspi->remaining_bytes -= wsize;
		sr = xspi_read8(xspi, XSPI_SR_OFFSET);
	}
}

static int xilinx_spi_txrx_bufs(struct spi_device *spi, struct spi_transfer *t)
{
	struct xilinx_spi *xspi = spi_master_get_devdata(spi->master);
	u32 ipif_ier;
	u16 cr;

	/* We get here with transmitter inhibited */

	xspi->tx_ptr = t->tx_buf;
	xspi->rx_ptr = t->rx_buf;
	xspi->remaining_bytes = t->len;
	INIT_COMPLETION(xspi->done);

	xilinx_spi_fill_tx_fifo(xspi);

	/* Enable the transmit empty interrupt, which we use to determine
	 * progress on the transmission.
	 */
	ipif_ier = xspi_read32(xspi, XIPIF_V123B_IIER_OFFSET);
	xspi_write32(xspi, XIPIF_V123B_IIER_OFFSET,
		 ipif_ier | XSPI_INTR_TX_EMPTY);

	/* Start the transfer by not inhibiting the transmitter any longer */
	cr = xspi_read16(xspi, XSPI_CR_OFFSET) & ~XSPI_CR_TRANS_INHIBIT;
	xspi_write16(xspi, XSPI_CR_OFFSET, cr);

	wait_for_completion(&xspi->done);

	/* Disable the transmit empty interrupt */
	xspi_write32(xspi, XIPIF_V123B_IIER_OFFSET, ipif_ier);

	return t->len - xspi->remaining_bytes;
}

/* This driver supports single master mode only. Hence Tx FIFO Empty
 * is the only interrupt we care about.
 * Receive FIFO Overrun, Transmit FIFO Underrun, Mode Fault, and Slave Mode
 * Fault are not to happen.
 */
static irqreturn_t xilinx_spi_irq(int irq, void *dev_id)
{
	struct xilinx_spi *xspi = dev_id;
	u32 ipif_isr;

	/* Get the IPIF interrupts, and clear them immediately */
	ipif_isr = xspi_read32(xspi, XIPIF_V123B_IISR_OFFSET);
	xspi_write32(xspi, XIPIF_V123B_IISR_OFFSET, ipif_isr);

	if (ipif_isr & XSPI_INTR_TX_EMPTY) {	/* Transmission completed */
		u16 cr;
		u8 sr;
		u8 rsize;
		if (xspi->bits_per_word == 8)
			rsize = 1;
		else if (xspi->bits_per_word == 16)
			rsize = 2;
		else
			rsize = 4;

		/* A transmit has just completed. Process received data and
		 * check for more data to transmit. Always inhibit the
		 * transmitter while the Isr refills the transmit register/FIFO,
		 * or make sure it is stopped if we're done.
		 */
		cr = xspi_read16(xspi, XSPI_CR_OFFSET);
		xspi_write16(xspi, XSPI_CR_OFFSET, cr | XSPI_CR_TRANS_INHIBIT);

		/* Read out all the data from the Rx FIFO */
		sr = xspi_read8(xspi, XSPI_SR_OFFSET);
		while ((sr & XSPI_SR_RX_EMPTY_MASK) == 0) {
			u32 data;
			if (rsize == 1)
				data = xspi_read8(xspi, XSPI_RXD_OFFSET);
			else if (rsize == 2)
				data = xspi_read16(xspi, XSPI_RXD_OFFSET);
			else
				data = xspi_read32(xspi, XSPI_RXD_OFFSET);

			if (xspi->rx_ptr) {
				if (rsize == 1)
					*xspi->rx_ptr = data & 0xff;
				else if (rsize == 2)
					*(u16 *)(xspi->rx_ptr) = data & 0xffff;
				else
					*((u32 *)(xspi->rx_ptr)) = data;
				xspi->rx_ptr += rsize;
			}

			sr = xspi_read8(xspi, XSPI_SR_OFFSET);
		}

		/* See if there is more data to send */
		if (xspi->remaining_bytes > 0) {
			xilinx_spi_fill_tx_fifo(xspi);
			/* Start the transfer by not inhibiting the
			 * transmitter any longer
			 */
			xspi_write16(xspi, XSPI_CR_OFFSET, cr);
		} else {
			/* No more data to send.
			 * Indicate the transfer is completed.
			 */
			complete(&xspi->done);
		}
	}
	return IRQ_HANDLED;
}

struct spi_master *xilinx_spi_init(struct device *dev, struct resource *mem,
	u32 irq, s16 bus_num, u16 num_chipselect, u8 bits_per_word,
	bool big_endian)
{
	struct spi_master *master;
	struct xilinx_spi *xspi;
	int ret = 0;

	master = spi_alloc_master(dev, sizeof(struct xilinx_spi));

	if (master == NULL)
		return ERR_PTR(-ENOMEM);

	/* the spi->mode bits understood by this driver: */
	master->mode_bits = SPI_CPOL | SPI_CPHA;

	xspi = spi_master_get_devdata(master);
	xspi->bitbang.master = spi_master_get(master);
	xspi->bitbang.chipselect = xilinx_spi_chipselect;
	xspi->bitbang.setup_transfer = xilinx_spi_setup_transfer;
	xspi->bitbang.txrx_bufs = xilinx_spi_txrx_bufs;
	xspi->bitbang.master->setup = xilinx_spi_setup;
	init_completion(&xspi->done);

	if (!request_mem_region(mem->start, resource_size(mem),
		XILINX_SPI_NAME)) {
		ret = -ENXIO;
		goto put_master;
	}

	xspi->regs = ioremap(mem->start, resource_size(mem));
	if (xspi->regs == NULL) {
		ret = -ENOMEM;
		dev_warn(dev, "ioremap failure\n");
		goto map_failed;
	}

	master->bus_num = bus_num;
	master->num_chipselect = num_chipselect;

	xspi->mem = *mem;
	xspi->irq = irq;
	xspi->bits_per_word = bits_per_word;
	xspi->big_endian = big_endian;

	/* SPI controller initializations */
	xspi_init_hw(xspi);

	/* Register for SPI Interrupt */
	ret = request_irq(xspi->irq, xilinx_spi_irq, 0, XILINX_SPI_NAME, xspi);
	if (ret != 0)
		goto unmap_io;

	ret = spi_bitbang_start(&xspi->bitbang);
	if (ret != 0) {
		dev_err(dev, "spi_bitbang_start FAILED\n");
		goto free_irq;
	}

	dev_info(dev, "at 0x%08X mapped to 0x%08X, irq=%d\n",
		(u32)mem->start, (u32)xspi->regs, xspi->irq);
	return master;

free_irq:
	free_irq(xspi->irq, xspi);
unmap_io:
	iounmap(xspi->regs);
map_failed:
	release_mem_region(mem->start, resource_size(mem));
put_master:
	spi_master_put(master);
	return ERR_PTR(ret);
}
EXPORT_SYMBOL(xilinx_spi_init);

void xilinx_spi_deinit(struct spi_master *master)
{
	struct xilinx_spi *xspi;

	xspi = spi_master_get_devdata(master);

	spi_bitbang_stop(&xspi->bitbang);
	free_irq(xspi->irq, xspi);
	iounmap(xspi->regs);

	release_mem_region(xspi->mem.start, resource_size(&xspi->mem));
	spi_master_put(xspi->bitbang.master);
}
EXPORT_SYMBOL(xilinx_spi_deinit);

MODULE_AUTHOR("MontaVista Software, Inc. <source@mvista.com>");
MODULE_DESCRIPTION("Xilinx SPI driver");
MODULE_LICENSE("GPL");

